import java.util.Scanner;
public class Matrix {

	public static void main(String[] args) {
		int i = 0;					//Initialisierung der Z�hlervariable
		int quersumme;
		int eingabe;
		Scanner ns = new Scanner(System.in);
		
		System.out.print("Geben Sie eine Zahl zwischen 2 und 9 ein: ");	
	    eingabe = ns.nextInt();	//Eingabe 
	    while(eingabe > 9 || eingabe < 2) {		//Schleife f�r erneute Eingabeaufforderung fall der eingegebene Wert nicht zwischen 2 und 9 liegt
	    	System.out.print("Der eingegebene Wert liegt nicht zwischen 2 und 9. Versuchen Sie es noch einmal: ");
	    	eingabe = ns.nextInt();	//Eingabe 
	    }
	    
		while (i < 100) {
			quersumme = berechneQuersumme(i); //Methodenaufruf zur Berechnung der Quersumme
			if((i % 10) ==0) {
				System.out.printf("\n");
			}
			if(i<10) {
				if((i % eingabe) == 0 || eingabe == quersumme) {
					
				}else {			//damit die erste Zeile der Matrix richtig formatiert ist
				System.out.printf(" ");	
				}
			}
			if((i % eingabe) == 0 || eingabe == quersumme || eingabe == (i % 10) || eingabe == (i/10)) {
			System.out.print(" *   ");		// �berpr�fung ob die Eingabe enthalten/quersumme/ohne Reste teilbar ist und Ausgabe eines *
			}else {
				System.out.printf("%d   ",i);	//Ausgabe des Z�hlers
			}
			i++;		//Z�hler wird erh�ht
		}
	}
	public static int berechneQuersumme(int i) {	//Methode zur Berechnung der Quersumme
		int summe = 0;
		while (0 != i) {
			// addiere die letzte ziffer der uebergebenen zahl zur summe
			summe = summe + (i % 10);
			// entferne die letzte ziffer der uebergebenen zahl
			i = i / 10;
		}
		return summe;
	}

}
