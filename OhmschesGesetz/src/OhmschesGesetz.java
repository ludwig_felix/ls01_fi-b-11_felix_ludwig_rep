import java.util.Scanner;
public class OhmschesGesetz {

	public static void main(String[] args) {
		Scanner ns = new Scanner(System.in);
		double u = 0;
		double r = 0;
		double i = 0;
		char gesucht = 'a';
		while(gesucht != 'U' && gesucht != 'R' && gesucht != 'I') {
			System.out.println("Was wird gesucht ? Geben Sie U, R oder I ein");
			gesucht = ns.next().charAt(0);
			if(gesucht != 'U' && gesucht != 'R' && gesucht != 'I') {
				System.out.println("ung�ltige Eingabe. Versuch es nochmal");
			}
		}
		
		switch(gesucht) {
		case 'U':
			System.out.println("Geben Sie einen Wert f�r R ein: ");
			r = ns.nextDouble();
			System.out.println("Geben Sie einen Wert f�r I ein: ");
			i = ns.nextDouble();
			u = r * i;
			System.out.printf("U = R x I = %.2f x %.2f = %.2f",r , i, u);
			break;
			
		case 'R':
			System.out.println("Geben Sie einen Wert f�r U ein: ");
			u = ns.nextDouble();
			System.out.println("Geben Sie einen Wert f�r I ein: ");
			i = ns.nextDouble();
			r = u / i;
			System.out.printf("U = R x I = %.2f : %.2f = %.2f",u , i, r);
			break;
			
		case 'I':
			System.out.println("Geben Sie einen Wert f�r R ein: ");
			r = ns.nextDouble();
			System.out.println("Geben Sie einen Wert f�r U ein: ");
			u = ns.nextDouble();
			i = u / r;
			System.out.printf("U = R x I = %.2f : %.2f = %.2f",u , r, i);
			break;
			
		}

	}

}
