import java.util.Scanner;

public class Mittelwert {

	public static void main(String[] args) {
		double zahl;		//Variablen deklaration
		int i = 1;
		double m = 0;
		int zaehler;
		Scanner myScanner = new Scanner(System.in);
		do {				//Schleife, um sicher zu stellen, dass die eingegebene Zahl �ber 0 liegt
			System.out.println("Dieses Programm berechnet den Mittelwert mehrerer Zahlen, wie viele Zahlen m�chten sie eingeben? ");
			zaehler = myScanner.nextInt();				//Eingabeaufforderung
			if(zaehler < 1) {
				System.out.println("Sie m�ssen mindestens eine Zahl eingeben. Versuchen Sie es nochmal!");
			}
		}while(zaehler < 1);
		
		do {			//Schleife, welche so oft durchlaufen wird wie der Benutzer eingegeben hat
			zahl = eingabe(myScanner, "Bitte geben Sie die erste Zahl ein: ");
			m = m + zahl;		//bildet die Summe der eingegebenen Zahlen
			i++;
		}while (i <= zaehler);
		
		m = m/zaehler;			//teilt die Summe der eingegebenen Zahlen durch ihre Anzahl
		ausgabe(m);				//Methodenaufruf zur Ausgabe
		
		myScanner.close();
	}
	
	public static double eingabe(Scanner ms, String text ) {	//liest eine Zahl ein
		System.out.print(text);
		double zahl = ms.nextDouble();
		return zahl;
	}
	
	public static void ausgabe(double mittelwert) {		//Gibt den �bergebene Wert aus
		System.out.println("Der errechnete Mittelwert ist: " + mittelwert);
	}
}
