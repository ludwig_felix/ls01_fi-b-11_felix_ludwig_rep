import java.util.Scanner; // Import der Klasse Scanner 
 
public class Rechner  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
     
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
     
    // Die Variable zahl1 speichert die erste Eingabe 
    float zahl1 = myScanner.nextInt();  
     
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
     
    // Die Variable zahl2 speichert die zweite Eingabe 
    float zahl2 = myScanner.nextInt();  
     
    // ich habe mich dazu entschieden alle Variablen zum Datentyp float zu �ndern um auch Gleitkommazahlen benutzen zu k�nnen   
    
    float ergebnisAddition = zahl1 + zahl2;  // Addition der Variablen zahl1 und zahl2 ; Speichern des Ergebnisses in neuer Variable ergebnisAddition
    float ergebnisSubtraktion = zahl1 - zahl2;// Subtraktion der Variablen zahl1 und zahl2 ; Speichern des Ergebnisses in neuer Variable ergebnisSubtraktion
    float ergebnisMultiplikation = zahl1 * zahl2;// Multiplikation der Variablen zahl1 und zahl2 ; Speichern des Ergebnisses in neuer Variable ergebnisMultiplikation
    float ergebnisDivision = zahl1 / zahl2;// Division der Variablen zahl1 und zahl2 ; Speichern des Ergebnisses in neuer Variable ergebnisDivision
     
    System.out.print("\n\n\nErgebnis der Addition lautet: "); 
    System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnisAddition);   // Ausgabe des Ergebnisses der Addition
 
    System.out.print("\n\n\nErgebnis der Subtraktion lautet: "); 
    System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnisSubtraktion);// Ausgabe des Ergebnisses der Subtraktion
    
    System.out.print("\n\n\nErgebnis der Multiplikation lautet: "); 
    System.out.print(zahl1 + " x " + zahl2 + " = " + ergebnisMultiplikation);// Ausgabe des Ergebnisses der Multiplikation
    
    System.out.print("\n\n\nErgebnis der Division lautet: "); 
    System.out.print(zahl1 + " : " + zahl2 + " = " + ergebnisDivision);// Ausgabe des Ergebnisses der Division
    
    myScanner.close(); 
     
  }    
}