
public class Temperaturtabelle_Ludwig {

	public static void main(String[] args) {
		String z = "Fahrenheit";  //die Namen in Variablen zum besseren Anordnen in der Ausgabe
		String y = "Celsius";
		double a = -20;		//die Variablen in Celsius
		double b = -10;
		double c = 0;
		double d = 20;
		double e = 30;
		
		
		System.out.printf("%-12s|%10s", z, y);			//Ausgabe der Namen
		System.out.println("\n-----------------------");		//die "Trennlinie" der Tabelle
		System.out.printf("%-12.2f|%10.2f", a, ((a - 32) * 5/9)); //Ausgaben der Variablen mit Umrechnung von Celsius zu Fahrenheit
		System.out.printf("\n%-12.2f|%10.2f", b, ((b - 32) * 5/9));//((a - 32) * 5/9) Umrechnungsformel von Celsius zu Fahrenheit
		System.out.printf("\n%+-12.2f|%10.2f", c, ((c - 32) * 5/9));
		System.out.printf("\n%+-12.2f|%10.2f", d, ((d - 32) * 5/9));
		System.out.printf("\n%+-12.2f|%10.2f", e, ((e - 32) * 5/9));
		
		
	}

}
