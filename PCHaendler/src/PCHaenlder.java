import java.util.Scanner;

public class PCHaenlder {

	public static void main(String[] args) {
		
		
	String artikel;
	int anzahl;
	double preis;
	double nettogesamtpreis;
	double bruttogesamtpreis;
	double mwst;
		
	Scanner myScanner = new Scanner(System.in);
	
	artikel = liesString(myScanner, "Was moechten Sie bestellen?");
	anzahl = liesInt(myScanner, "Geben Sie die Anzahl ein:");
	preis = liesDouble(myScanner, "Geben Sie den Nettopreis ein:");
	nettogesamtpreis = berechneGesamtnettopreis (anzahl, preis);
	mwst = liesMwst(myScanner, "Geben Sie den Mehrwertsteuersatz in Prozent ein:");
	bruttogesamtpreis = berechneGesamtbruttopreis (nettogesamtpreis, mwst);

	rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
			
	myScanner.close();
	
	}




public static String liesString(Scanner ms, String text) {
	
	System.out.println(text);
	String artikel = ms.next();
	return artikel;
	
}


public static int liesInt(Scanner ms, String text) {
	
	System.out.println(text);
	int anzahl = ms.nextInt();
	return anzahl;
	
}

public static double liesDouble(Scanner ms, String text) {
	
	System.out.println(text);
	double preis = ms.nextDouble();
	return preis;
}

public static double berechneGesamtnettopreis( int anzahl, double preis) {
	
	 double nettogesamtpreis = anzahl * preis;
	 return nettogesamtpreis;
}

public static double liesMwst(Scanner ms, String text) {
	System.out.println(text);
	double mwst = ms.nextDouble();
	return mwst;
}

public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
	
	
	double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
	return bruttogesamtpreis;
}

public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		
System.out.println("\tRechnung");
System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");


}
}
//public static double liesDouble(String text)
//publicstaticdoubleberechneGesamtnettopreis(intanzahl, double
//nettopreis)
//publicstaticdoubleberechneGesamtbruttopreis(doublenettogesamtpreis,
//doublemwst)
//public static void rechungausgeben(String artikel, int anzahl, double 
//nettogesamtpreis, double bruttogesamtpreis,
//double mwst)