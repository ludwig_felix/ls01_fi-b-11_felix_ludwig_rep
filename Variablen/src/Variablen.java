//package eingabeAusgabe
/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author
    @version
*/

public class Variablen {
  public static void main(String [] args){
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */
	  int zaehler = 0; 	// Variable zum Z�hlen der Programmdurchl�ufe;
	  					// erstmal als int da nicht klar ist wie oft das Programm durchlaufen wird
	  zaehler++;		// die Variable um 1 erh�hen um die Durchl�ufe des Programms zu z�hlen

	  
    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	  zaehler = 25;		// Setzt die Variable zaehler auf den Wert 25
	  System.out.println("Anzhahl der Programmdurchl�ufe: " + zaehler);	// Gibt die Anzahl der Programmdurchl�ufe aus

	  
    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */
	  
	  char menuepunkt;	// char weil nur ein Buchstabe; die Variable bekommt den eingegebenden wert
	    

    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	  menuepunkt = 'C';			// Zuweisung der char Variable menuepunkt auf den "Wert" C
	  System.out.println("Ihre Eingabe lautet: " + menuepunkt);	// Ausgabe der Variable menuepunkt

	  
    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
	  int astroBerechnung ;	// Erstellung der Variable astroBerechnung mit dem Datentyp long f�r extra gro�e Zahlen

	  
    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
	  astroBerechnung = 299792458 ; // Setzt wert auf Lichtgeschwindigkeit
	  System.out.printf("Die Lichtgeschwindigkeit betr�gt im Luftleeren Raum %d Meter pro Sekunde\n", astroBerechnung);	// gibt die Variable astroBerechnung aus
	  
	  
    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
	  byte anzMitglieder = 7;	// Erstellung und Initialisierung der Variable anzMitglieder mit Datentyp byte, falls die Anzahl der Mitglieder den Wert von 127 �berschreitet muss der Datentyp auf short ge�ndert werden

	  
    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
	  System.out.println("Anzahl der Mitglieder: " +anzMitglieder);

	  
    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
	  float ladung = 0.0000000000000000001602f;	// Initialisierung der Variable ladung mit dem Wert der Elementarladung; float weil der Wert mit e^-19 noch in den float mit einer Gr��e von e^-45 passt
	  System.out.printf("Die elektrische Elementarladung ist: %.22f Coulomb " ,ladung );	//Ausgabe der Variable ladung
	  

    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
	  boolean zahlungseingang; // einen Zahlung kann entweder Erfolgt sein oder nicht daher bietet sich der Datentyp boolean an

	  
    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
	  zahlungseingang = true ;	// zuweisung der Variable auf true, da die Zahlung eingegangen ist
	  System.out.println("Status des Zahlungseingangs: " + zahlungseingang ); // Ausgabe des Zahlungseingangsstatuses

  }//main
}// Variablen