import java.util.Scanner;//mit java.util.* werden alle importiert
public class HelloWorld2 {

	public static void main(String[] args) {
		
		Scanner myscanner = new Scanner(System.in); 
		
		float zahl1, zahl2, summe ;
		String name;
		double zahlDouble;
		long zahlLong;
		byte zahlByte;
		short zahlShort;
		boolean wahrheitswert;
		char buchstabe;
		
//		System.out.println("Das Programm addiert zwei Zahlen miteinander");
//		System.out.println("bitte geben Sie die erste Zahl ein: ");
//		zahl1 = myscanner.nextFloat();
//		
//		System.out.println("bitte geben Sie die zweite Zahl ein: ");
//		zahl2 = myscanner.nextFloat();
//		
//		summe = zahl1 + zahl2;
//		System.out.println("das ergebnis ist gleich: " +summe);

		
//		System.out.println("Geben Sie Ihren Namen ein: ");
//		name = myscanner.next();
//		System.out.println("Ihr Name lautet " +name); 
		
		System.out.println("Double: ");
		zahlDouble = myscanner.nextDouble();
		System.out.println("Ihre Eingabe" +zahlDouble); 

		System.out.println("Short: ");
		zahlShort = myscanner.nextShort();
		System.out.println("Ihre Eingabe" +zahlShort);
		
		System.out.println("Byte: ");
		zahlByte = myscanner.nextByte();
		System.out.println("Ihre Eingabe" +zahlByte);
		
		System.out.println("Long: ");
		zahlLong = myscanner.nextLong();
		System.out.println("Ihre Eingabe: " +zahlLong);
		
		System.out.println("Trifft die Aussage zu ?");
		wahrheitswert = myscanner.nextBoolean();
		System.out.println("Sie haben");
		
		System.out.println("Geben Sie eine Zeichen ein: ");
		buchstabe = myscanner.next().charAt(0);
		System.out.println("Ihre Eingabe: " +buchstabe);
		
		
		myscanner.close();

	}

}
