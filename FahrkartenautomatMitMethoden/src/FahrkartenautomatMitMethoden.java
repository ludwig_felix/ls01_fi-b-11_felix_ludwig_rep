
import java.util.Scanner;
class FahrkartenautomatMitMethoden {

	public static void main(String[] args) {
		
		while(true){
			
			double zuzahlenderBetrag = fahrkartenBestellungErfassen();			//Aufruf von fahrkartenBestellungErfassen		
			double rueckgabeBetrag = fahrkartenBezahlen(zuzahlenderBetrag);		//Aufruf von fahrkartenBezahlen mit uebergabe von zuzahlenderBetrag		
			fahrscheinausgeben();					//Aufruf von fahrscheinausgeben		
			rueckgeldAusgeben(rueckgabeBetrag);		//Aufruf von rueckgeldAusgeben mit uebergabe von rueckgabeBetrag
		}
	}

		public static double fahrkartenBestellungErfassen()						{
			double preis = 0;
			int auswahl = 0;
			int anzahlTickets;
			double zuZahlenderBetrag = 0;
			boolean janein = true;
			char eingabe = 'a';
			Scanner ns = new Scanner(System.in);
			
			while(janein == true) {
			do {
				System.out.print("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:\r\n"
					+ "  (1) Einzelfahrschein Regeltarif AB [3,00 EUR]\r\n"
					+ "  (2) Tageskarte Regeltarif AB [8,80 EUR] \r\n"
					+ "  (3) Kleingruppen-Tageskarte Regeltarif AB [25,50 EUR] \r\n"
					+ "      Welche m�chte Sie haben?");		
				auswahl = ns.nextInt();
				
					if(auswahl > 3 || auswahl < 1 ) {
						System.out.println("\nDie eingegebene Zahl muss zwischen 1 und 3 liegen. Versuchen Sie es nochmal.\r\n");
					}
				
			}while(auswahl > 3 || auswahl < 1);
	
			
			
				switch(auswahl) {
				case 1:
					preis = 3.00;
					break;
				
				case 2:
					preis = 8.80;
					break;
				
				case 3:
					preis = 25.50;
					break;
			}
			

			do {
				System.out.print("Geben Sie die Anzahl der Tickets ein: ");		
				anzahlTickets = ns.nextInt();
			    if(anzahlTickets < 1 || anzahlTickets > 10) {
			    	System.out.println("Die eingegebene Zahl muss zwischen 1 und 10 liegen. Versuchen Sie es nochmal.");
			    }
				}while(anzahlTickets < 1 || anzahlTickets > 10);
			
			
			zuZahlenderBetrag += preis*anzahlTickets;
			eingabe = 'a';
			while(eingabe != 'j'&& eingabe != 'n') {
			System.out.println("m�chten Sie noch weitere Fahrkarten bestellen? geben Sie j f�r ja und n f�r nein ein.");
			eingabe = ns.next().charAt(0);
			if(eingabe == 'j') {
				janein = true;
			}
			if(eingabe == 'n') {
				janein = false;
			}
			if(eingabe != 'j' && eingabe != 'n') {
				System.out.println("Ung�ltige Eingabe. Versuchen Sie es nochmal ");
			}
			}
			
			
			}
			
		    return zuZahlenderBetrag;
			
			
				
		}
		
		public static double fahrkartenBezahlen(double zuzahlenderBetrag) 		// Methode welche mit Eingabeaufforderung den
		{																		// Eingezahlten Betrag ermittelt und den
				float eingezahlterGesamtbetrag = 0.0f;							// Rueckgabebetrag zurueckgibt
				float eingeworfeneMuenze;
				Scanner ns = new Scanner(System.in);
		       while(eingezahlterGesamtbetrag < zuzahlenderBetrag)
		       {
		    	   System.out.printf("Noch zu zahlen: %.2f Euro\n" , (zuzahlenderBetrag - eingezahlterGesamtbetrag));
		    	   System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
		    	   eingeworfeneMuenze = ns.nextFloat();
		           eingezahlterGesamtbetrag += eingeworfeneMuenze;
		       }
		      
		       return eingezahlterGesamtbetrag - zuzahlenderBetrag;
		}
		public static void fahrscheinausgeben() 								// Methode zur fahrscheinausgabe
		{																		// Wartet lediglich und gibt Text aus
			 System.out.println("\nFahrschein wird ausgegeben");				// Gibt keinen Wert zurueck
		       for (int i = 0; i < 8; i++)
		       {
		          System.out.print("=");
		          try {
					Thread.sleep(250);
				  } catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				    }
		       }
		       System.out.println("\n\n");			
		}
		public static void rueckgeldAusgeben(double rueckgabeBetrag) 			// Methode welche den Rueckgeld Betrag 
		{																		// ueber schleifen ermittelt und ausgibt
			if(rueckgabeBetrag > 0.0)											// Gibt keinen Wert zurueck
		       {
		    	   System.out.printf("Der Rueckgabebetrag in Hoehe von %.2f EURO" , rueckgabeBetrag );
		    	   System.out.println("\nwird in folgenden Muenzen ausgezahlt:");
		           while(rueckgabeBetrag >= 2.0) // 2 EURO-Muenzen
		           {
		        	  System.out.println("2 EURO");
		        	  rueckgabeBetrag = runden(rueckgabeBetrag -= 2.0);
		           }
		           while(rueckgabeBetrag >= 1.0) // 1 EURO-Muenzen
		           {
		        	  System.out.println("1 EURO");
		        	  rueckgabeBetrag = runden(rueckgabeBetrag -= 1.0);
		           }
		           while(rueckgabeBetrag >= 0.5) // 50 CENT-Muenzen
		           {
		        	  System.out.println("50 CENT");
		        	  rueckgabeBetrag = runden(rueckgabeBetrag -= 0.5);
		           }
		           while(rueckgabeBetrag >= 0.2) // 20 CENT-Muenzen
		           {
		        	  System.out.println("20 CENT");
		        	  rueckgabeBetrag = runden(rueckgabeBetrag -= 0.2);
		           }
		           while(rueckgabeBetrag >= 0.1) // 10 CENT-Muenzen
		           {
		        	  System.out.println("10 CENT");
		        	  rueckgabeBetrag = runden(rueckgabeBetrag -= 0.1);
		           }
		           while(rueckgabeBetrag >= 0.05)// 5 CENT-Muenzen
		           {
		        	  System.out.println("5 CENT");
		        	  rueckgabeBetrag = runden(rueckgabeBetrag -= 0.05);
		           }
		       }
			System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                    "vor Fahrtantritt entwerten zu lassen!\n"+
                    "Wir wuenschen Ihnen eine gute Fahrt.\n"
                    + "\n------------------------------------------\n");
		}
		public static double runden(double zahl) {
			zahl = Math.round(zahl * 100)/100.00;
			//System.out.println("Extra: " + zahl);
			return zahl;
		}
}
