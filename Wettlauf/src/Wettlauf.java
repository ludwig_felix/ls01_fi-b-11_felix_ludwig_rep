
public class Wettlauf {

	public static void main(String[] args) {
		String a = "Sprinter A";
		String b = "Sprinter B";
		double speedA = 9.5;
		double speedB = 7;
		double distanzA = 0;
		double distanzB = 250;
		int sek = 0;
		
		System.out.printf("%-12s|%12s", a, b);			//Ausgabe der Namen
		System.out.println("\n-------------------------");		//die "Trennlinie" der Tabelle
		System.out.printf("%3d |%9.2f |%9.2f\n", sek, distanzA, distanzB);
		do {
			distanzA += speedA;		//Erh�hung der zur�ckgelegten Distanzen um die Geschwindigkeit der Sprintr
			distanzB += speedB;
			sek++;				// Erh�hung des Sekundenz�hlers
			System.out.printf("%3d |%9.2f |%9.2f\n", sek, distanzA, distanzB);	//Ausgabe der Sekundenzahl und der zur�ckgelegten Distanzen der beiden Sprinter
		} while (distanzA <= 1000 && distanzB <= 1000);		//Abbruchbedingung, eine der zur�ckgelegten Distanzen muss 1000 sein
		if(distanzA >= 1000) {		//Ausgabe des Siegers
			System.out.printf("Der Sieger ist %s. Er erreichte das Ziel in %d Sekunden", a, sek);
		}else {
			System.out.printf("Der Sieger ist %s. Er erreichte das Ziel in %d Sekunden", b, sek);
		}

	}

}
